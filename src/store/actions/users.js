import { 
  GET_USERS, 
  ERROR_USER, 
  CREATED_USER,
  DELETED_USER
} from '../definitions'

import { UserService } from '../../api'

const userModel = {
  id: '',
  name: '',
  photo: '',
  description: ''
}

export function usersGetAllAction() {
  return async (dispatch) => {
    try {
      let usersList = await UserService.getAll()
      dispatch({ type: GET_USERS, payload: usersList })
    } catch (error) {
      dispatch({type: ERROR_USER })
    }
  }
}

export function userAddNewAction(_user_) {
  userModel.photo = _user_.photo
  userModel.name = _user_.name
  userModel.description = _user_.description
  
  let user = Object.freeze(Object.assign({}, userModel))
  return async (dispatch) => {
    try {
      let userList = await UserService.create(user)
      dispatch({ type: CREATED_USER, payload: userList})
    } catch (error) {
      dispatch({type: ERROR_USER })
    }
  }
}

export function userDeleteAction(_userID) {
  return async (dispatch) => {
    try {
      dispatch({ type: DELETED_USER, payload: _userID})
    } catch (error) {
      dispatch({type: ERROR_USER })
    }
  }
}