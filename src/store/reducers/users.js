import { 
  CREATED_USER, 
  DELETED_USER, 
  GET_USERS, 
  ERROR_USER 
} from '../definitions'

const initialState  = {
  users: [],
  error: ''
}

const users = (state = initialState, action) => {
  let users = [...state.users]
  switch(action.type) {
    case CREATED_USER:
      users.push(action.payload)
      return {...state, users: users}
    case DELETED_USER:
      let index = users.findIndex(item => item.id === action.payload)
      users.splice(index,1)
      return {...state, users: users}
    case GET_USERS:
      return {...state, users: action.payload}
    case ERROR_USER:
      return {...state, error: action.payload}
    default:
      return state
  }
}

export default users