import { endpoint } from '../config'

class UserService {
  constructor() {
    this.endpoint = `${endpoint}api/users`
  }
  
  getAll = async () => {
    console.log('[USERSERVICE]', 'get all users')
    let options = {
      headers: {
        'Content-type': 'application/json'
      },
      method: 'GET'
    }
    
    let request = await fetch(this.endpoint, options)
    return request.json()
  }
  
  create = async (_user) => {
    console.log('[USERSERVICE]', 'created new user')
    let options = {
      headers: {
        'Content-type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(_user)
    }

    let request = await fetch(this.endpoint, options)
    return request.json()
  }
  
  delete = async (userID) => {
    console.log('[USERSERVICE]', 'deleted user')

    let options = {
      headers: {
        'Content-type': 'application/json'
      },
      method: 'DELETE'
    }

    let request = await fetch(`${endpoint}/${userID}`, options)
    return request.json()
  }
}

export default new UserService()