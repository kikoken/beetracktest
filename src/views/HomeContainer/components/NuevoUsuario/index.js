import React from 'react'

import './nuevousuario.css'
const NuevoUsuario = (props) => props.visible && (
  <section id="wrapper">
    <div className="modal">
      <header><h3>Agregar nuevo contacto</h3></header>
      <form method="POST" onSubmit={(e) => props.onSubmit(e)}>
        <div className="form-input">
          <label htmlFor="url">URL Imagen de perfil</label>
          <input 
            type="url" 
            name="photo"
            patern="https://*"
            placeholder="https://miimagen.ima" 
            required/>
        </div>

        <div className="form-input">
          <label htmlFor="name">Nombre</label>
          <input 
            type="text" 
            name="name"
            placeholder="Ingresa un nombre" 
            required/>
        </div>

        <div className="form-input">
          <label htmlFor="description">Descripción</label>
          <input 
            type="text" 
            name="description" 
            required/>
        </div>

        <button type="submit">Guardar</button>
      </form>
    </div>
  </section>
)

export default NuevoUsuario