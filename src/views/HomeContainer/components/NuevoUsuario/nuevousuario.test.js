import React from 'react'
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

import NuevoUsuario from './index'

Enzyme.configure({ adapter: new Adapter() })


describe('NuevoUsuario', () => {
  let wrapper, submitMock

  beforeEach(() => {
    submitMock = jest.fn()

    wrapper = shallow(<NuevoUsuario visible={true} onSubmit={submitMock} />)
  })
  it('render NuevoUsuario component', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('UI nuevo usuario', () => {
    expect(wrapper.find('h3').text()).toEqual('Agregar nuevo contacto')
  })

  it('Submit event', () => {
    wrapper.find('form').simulate('submit')
    expect(submitMock).toBeCalledTimes(1)
  })
})