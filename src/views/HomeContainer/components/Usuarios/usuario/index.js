import React from 'react'

const UserItem = (props) => (
  <div className="user">
    <div className="shadow-box">
      <div className="user--image">
        <figure>
          <img src={props.user.photo} alt={props.user.name}/>
        </figure>
      </div>
      <div className="user-info">
        <h5>{props.user.name}</h5>
        <a href="https://" onClick={e => props.handlerDeleteUser(e, props.user.id)} >Eliminar</a>
      </div>
    </div>
    <div className="shadow-box">
      <p>{props.user.description}</p>
    </div>
  </div>
)

export default UserItem