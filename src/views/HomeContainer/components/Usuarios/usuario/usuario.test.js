import React from 'react'
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

import UsuarioItem from './index'

Enzyme.configure({ adapter: new Adapter() })


describe('UsuarioItem', () => {
  let wrapper, user, handlerMock

  beforeEach(() => {
    user = {
      "id": 1,
      "name": "Francisco",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
      "photo": "https://images.pexels.com/photos/91227/pexels-photo-91227.jpeg?h=350&auto=compress&cs=tinysrgb"
    }
    handlerMock = jest.fn()

    wrapper = shallow(<UsuarioItem user={user} handlerDeleteUser={handlerMock} />)
  })
  it('render Usuario component', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('Load data user', () => {
    expect(wrapper.find('img').prop('alt')).toEqual(user.name)
    expect(wrapper.find('h5').text()).toEqual(user.name)
  })

  it('Click event handlerDeleteUser', () => {
    wrapper.find('a').simulate('click')
    expect(handlerMock).toBeCalledTimes(1)
  })
})