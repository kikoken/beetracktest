import React from 'react'
import './usuarios.css'

import UserItem from './usuario'

const Usuarios = (props) => (
  <article className="usuarios">
    <header className="shadow-box">
      <h3>Nombre</h3>
      <h3>Descripción</h3>
    </header>
    {props.users.map((item) => (
      <UserItem key={item.id} user={item} handlerDeleteUser={props.handlerDeleteUser} />    
    ))}
  </article>
)

export default Usuarios