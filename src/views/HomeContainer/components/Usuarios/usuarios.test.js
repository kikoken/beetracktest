import React from 'react'
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'

import Usuarios from './index'
import Data from '../../../../../db.json'

Enzyme.configure({ adapter: new Adapter() })


describe('Usuarios', () => {
  let wrapper, users, handlerMock

  beforeEach(() => {
    users = Data.users
    handlerMock = jest.fn()

    wrapper = shallow(<Usuarios users={users} handlerDeleteUser={handlerMock} />)
  })
  it('render Usuarios component', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('render Usuario Item', () => {
    expect(wrapper.find('UserItem')).toHaveLength(6)
  })
})