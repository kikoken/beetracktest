import React, { Component } from 'react'
import { connect } from 'react-redux'

import './home.css'

import Usuarios from './components/Usuarios'
import Button from '../../components/Button'
import NuevoUsuario from './components/NuevoUsuario'

import { 
  usersGetAllAction, 
  userAddNewAction,
  userDeleteAction 
} from '../../store/actions'

class HomeContainer extends Component {
  constructor() {
    super()
    this.state = {
      search: '',
      showModal: false
    }
  }
  async componentDidMount() {
    await this.props.usersGetAllAction()
  }

  handlerDeleteUser = async (e, userId) => {
    e.preventDefault()
    await this.props.userDeleteAction(userId)
  }

  updateSearch = (e) => {
    this.setState({search: e.target.value})
  }
  
  switchModal = () => {
    this.setState({showModal: !this.state.showModal})
  }

  onSubmitNewUser = async (e) => {
    e.preventDefault()
    let user = {
      name: e.target.name.value,
      photo: e.target.photo.value,
      description: e.target.description.value
    }

    await this.props.userAddNewAction(user)
    this.switchModal()
  }
  render() {
    let filteredUsers = this.props.users.users.filter(user => user.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1)
    return(
      <section>
        <NuevoUsuario visible={this.state.showModal} onSubmit={this.onSubmitNewUser} />
        <div className="container">
          <header>
            <h1>Test <b>Beetrack</b> </h1>
            <div className="filtro">
              <div>
                <i className="fas fa-search"></i> 
                <input 
                  type="search" 
                  name="search" 
                  value={this.state.search} 
                  onChange={e => this.updateSearch(e)} />
              </div>
              <div><Button label="Nuevo Contacto" add={true} onClick={this.switchModal} /></div>
            </div>
          </header>
        
          <Usuarios users={filteredUsers} handlerDeleteUser={this.handlerDeleteUser} />
          
           <footer>
            <Button label="Siguiente página" page={true} />
          </footer>
        </div>
        

      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

export default connect(mapStateToProps, { usersGetAllAction, userAddNewAction, userDeleteAction } )(HomeContainer)