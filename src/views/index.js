import React from 'react'
import {
  BrowserRouter,
  Route
} from 'react-router-dom'

import HomeContainer from './HomeContainer'

export default () => (
  <BrowserRouter>
    <Route path="/" component={HomeContainer}/>
  </BrowserRouter>
)