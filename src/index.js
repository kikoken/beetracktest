import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import Store from './store';

import './styles/index.css';
import Main from './views';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={Store}> 
    <Main /> 
  </Provider>, 
  document.getElementById('root')
);
serviceWorker.unregister();
