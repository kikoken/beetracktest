import React from 'react'

import './button.css'

const Button = (props) => (
  <button onClick={props.onClick} className={props.page ? 'btn-page': ''} >
    {props.add 
      ? <i className="fas fa-plus-circle"></i>
      : null 
    }  
    { props.label }
    {props.page 
      ? <i className="fas fa-arrow-alt-circle-right"></i>
      : null 
    } 
  </button>
)

export default Button